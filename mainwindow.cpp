#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QCoreApplication>
#include <opencv2/opencv.hpp>
#include <qstring.h>

using namespace cv;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this); // Ui Setup
    ui->stackedWidget->setCurrentIndex(0); // StackedWidget 0th Index


}

MainWindow::~MainWindow()
{
    delete ui;
}


//Pages pushButtons
void MainWindow::on_pg1_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);

}
void MainWindow::on_pg2_clicked()
{
     ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::on_pg3_clicked()
{
     ui->stackedWidget->setCurrentIndex(3);
}

void MainWindow::on_pg4_clicked()
{
     ui->stackedWidget->setCurrentIndex(4);
}


//Main Menu PushButtons
void MainWindow::on_pg1menu_clicked()
{
     ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_pg2menu_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_pg3menu_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_pg4menu_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}



//page 3
void MainWindow::on_pushButton_clicked()
{
    VideoCapture capture(1);

    while( capture.isOpened() )
    {
        Mat frame;
        if ( ! capture.read(frame) )
            continue;
        imshow("Camera",frame);
        if ( waitKey(10)==27 )
            break;
    }
}



//Page 4
void MainWindow::on_FPS_clicked()
{

     VideoCapture capture(1);

    if(capture.isOpened())
    {
        frame_width = static_cast<int>(capture.get(CAP_PROP_FRAME_WIDTH)); //get the width of frames of the video
        frame_height = static_cast<int>(capture.get(CAP_PROP_FRAME_HEIGHT)); //get the height of frames of the video
        frame_rate = static_cast<int>(capture.get(CAP_PROP_FPS)); //get the fps of the video
        QString string1 = QString::number(frame_width); //Converting int to Qstring
        QString string2 = QString::number(frame_height);
        QString string3 = QString::number(frame_rate);

        ui->label1->setText(string1); // Displays Frame_Width
        ui->label2->setText(string2);// Displays Frame_height
        ui->label3->setText(string3);// Displays Frame_Rate
        ui->label->setText(QString::number(frame_width)+"x"+ QString::number(frame_height));//Displays Resolution

    }
}



