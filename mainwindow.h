#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    void on_pg1_clicked();

    void on_pg2_clicked();

    void on_pg3_clicked();

    void on_pg4_clicked();

    void on_pg4menu_clicked();

    void on_pg3menu_clicked();

    void on_pg2menu_clicked();

    void on_pg1menu_clicked();

    void on_pushButton_clicked();

    void on_FPS_clicked();



private:
    Ui::MainWindow *ui;
    int frame_width;
    int frame_height;
    int frame_rate;


};
#endif // MAINWINDOW_H
